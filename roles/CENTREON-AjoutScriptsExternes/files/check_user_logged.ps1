$computername = "."
$str = ' '+$args[0]+' '
$numSessions =  $(query session | select-string -SimpleMatch $str).count

if ( $numSessions -eq 0) { 
    Write-Output "CRITICAL: $($args[0]) est deconnecte. Merci de relancer la session RDP"
    exit 2
} else {
    Write-Output "OK: $($args[0]) est connecte"
    exit 0
}